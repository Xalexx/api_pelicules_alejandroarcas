package com.example.routes

import com.example.models.Comment
import com.example.models.Film
import com.example.models.storedFilms
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.filmRouting() {
    route("/films") {
        get(""){
            if (storedFilms.isNotEmpty()) {
                call.respond(storedFilms)
            } else call.respondText("No films found", status = HttpStatusCode.OK)
        }
        get("/"){
            if (storedFilms.isNotEmpty()) {
                call.respond(storedFilms)
            } else call.respondText("No films found", status = HttpStatusCode.OK)
        }
        get("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) {
                return@get call.respondText("Missing ID", status = HttpStatusCode.BadRequest)
            }
            val id = call.parameters["id"]
            for (i in storedFilms.indices){
                if (storedFilms[i].id == id) return@get call.respond(storedFilms[i])
            }
            call.respondText("Film with id $id not found", status = HttpStatusCode.NotFound)
        }
        post {
            val filmToAdd = call.receive<Film>()
            storedFilms.add(filmToAdd)
            call.respondText("Film stored correctly", status = HttpStatusCode.Created)
        }
        put("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) {
                return@put call.respondText("Missing ID", status = HttpStatusCode.BadRequest)
            }
            val id = call.parameters["id"]
            val movieToUpdate = call.receive<Film>()
            for (film in storedFilms.indices){
                if (storedFilms[film].id == id){
                    storedFilms[film].title = movieToUpdate.title
                    storedFilms[film].year = movieToUpdate.year
                    storedFilms[film].genre = movieToUpdate.genre
                    storedFilms[film].director = movieToUpdate.director
                    return@put call.respondText("Film with id $id has been updated", status = HttpStatusCode.Accepted)
                }
            }
            call.respondText("Film with id $id not found", status = HttpStatusCode.NotFound)
        }
        delete("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) {
                return@delete call.respondText("Missing ID", status = HttpStatusCode.BadRequest)
            }
            val id = call.parameters["id"]
            for (film in storedFilms.indices){
                if (storedFilms[film].id == id) {
                    storedFilms.remove(storedFilms[film])
                    return@delete call.respondText("Film removed correctly", status = HttpStatusCode.Accepted)
                }
            }
            call.respondText("Film with id $id not found", status = HttpStatusCode.NotFound)
        }
        route("{id?}/comments"){
            post {
                val commentToAdd = call.receive<Comment>()
                if (call.parameters["id"].isNullOrBlank()) {
                    return@post call.respondText("Missing ID", status = HttpStatusCode.BadRequest)
                }
                val id = call.parameters["id"]
                for (i in storedFilms.indices){
                    if (storedFilms[i].id == id){
                        storedFilms[i].commentsList.add(commentToAdd)
                    }
                }
                call.respondText("Comment stored correctly", status = HttpStatusCode.Created)
            }
            get {
                if (call.parameters["id"].isNullOrBlank()) {
                    return@get call.respondText("Missing ID", status = HttpStatusCode.BadRequest)
                }
                val id = call.parameters["id"]
                for (i in storedFilms.indices){
                    if (storedFilms[i].id == id){
                        for (j in storedFilms[i].commentsList){
                            return@get call.respond(j)
                        }
                        call.respondText("Comments not found", status = HttpStatusCode.NotFound)
                    }
                }
                call.respondText("Film with id $id not found", status = HttpStatusCode.NotFound)
            }
        }
    }
}