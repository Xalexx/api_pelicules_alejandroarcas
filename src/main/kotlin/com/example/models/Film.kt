package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class Film(
    val id: String,
    var title: String,
    var year: String,
    var genre: String,
    var director: String,
    val commentsList: MutableList<Comment>
)

val storedFilms = mutableListOf<Film>()