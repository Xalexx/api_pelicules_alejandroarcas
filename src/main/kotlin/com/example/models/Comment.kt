package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class Comment(
    val id: String,
    val comment: String,
    val creationDate: String
)